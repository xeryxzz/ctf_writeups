# Hacking For Vodka
Category: Reverse

Points: 56

Description: We ran out of soju so you will have to hack for vodka now, smh my head! Anyways, good luck.

We are given the executable `vodka`, running this we get a prompt asking for a password.

Running `strace` on `vodka` indicates to us that there may be some debugger trap since it contains the system call for `ptrace`.

Let's have a look at the program in the Ghidra decompiler. We notice that Ghidra isn't able to identify the `main` function. Let's have a look at the unnamed functions and look for something interesting. In the function `FUN_001012df` we find the reference to `ptrace` and conditional logic which depends on `ptrace`s output. Let's follow the function which is run when we are not using a debugger (`FUN_001012bf`) and inside here we find the call to another function `FUN_00100c6d`.

The function `FUN_00100c6d` seems to be setting some kind of an alphabet which the program uses in the function call for `FUN_0010092a`.

Looking into `FUN_0010092a` we notice the calls to `fgets`, `strcmp` and the operator for XOR `^`. We also have several output statements both asking for a password and warning about incorrect password. Inside we find the following important code:
```c
fgets(local_58,local_7c + 1,__stream,*(undefined *)(&uStack144 + lVar3 * 0x1ffffffffffffffe));
(&uStack144)[lVar3 * 0x1ffffffffffffffe] = 0x100bd1;
strtok(local_58,&DAT_001013ea,*(undefined *)(&uStack144 + lVar3 * 0x1ffffffffffffffe));
local_64 = 0;
while (local_64 < local_7c) {
  local_4c = local_58[(long)local_64];
  local_4b = 0;
  local_4a = (byte)local_64 ^ *(byte *)(local_78 + (long)local_64); // XOR key becomes: 0x00, 0x01, 0x02 ... 0xff
  local_49 = 0;
  (&uStack144)[lVar3 * 0x1ffffffffffffffe] = 0x100c1d;
  iVar2 = strcmp(&local_4c,&local_4a,*(undefined *)(&uStack144 + lVar3 * 0x1ffffffffffffffe));
  if (iVar2 != 0) {
	(&uStack144)[lVar3 * 0x1ffffffffffffffe] = 0x100c2d;
	puts("Sorry, incorrect password!",*(undefined *)(&uStack144 + lVar3 * 0x1ffffffffffffffe));
				  /* WARNING: Subroutine does not return */
	(&uStack144)[lVar3 * 0x1ffffffffffffffe] = 0x100c37;
	exit(0,*(undefined *)(&uStack144 + lVar3 * 0x1ffffffffffffffe));
  }
  local_64 = local_64 + 1;
}
```

Notice that XOR key becomes: : `0x00`, `0x01`, `0x02` ... `0xff`. It seems like the `strcmp` is making a comparison between the XORed value and a value placed on the stack, let's run the program in `edb` and see if we can extract it. First we need to make sure we aren't falling into the debugger trap. We can do this by letting the program run to the address `0x0000563186CCA2F7` and then setting the instruction pointer (`RIP`) to `0x0000563186CCA312`. If we now let the program run to `strcmp` (`0x00005555BEBBAC18`) we should be able to extract the value from the stack at the address `0x00007FFE7DBC10B0`.

Now that we have extracted the value we could create the following program to get the flag:
```python
extracted_value = '\x77\x60\x76\x66\x72\x77\x7d\x73\x60\x3d\x64\x60\x39\x52\x66\x3b\x73\x7a\x23\x7d\x73\x4a\x70\x78\x6a\x46\x69\x2b\x76\x68\x41\x77\x41\x42\x49\x4a\x4a\x42\x40\x48\x5a\x5a\x45\x41\x59\x03\x5a\x4a\x51\x5c\x4f'

for i in range(len(extracted_value)):
	print(chr(ord(extracted_value[i]) ^ i), end='')
```

Flag: `watevr{th4nk5_h4ck1ng_for_s0ju_hackingforsoju.team}`
