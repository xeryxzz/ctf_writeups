# Fuckn't
Category: Reverse

Points: 111

Description: Fuck. Not only did I lose the flag, but my code also turned sentient again.

We are given the file `fuck.js` which has the following content:
```javascript
[][(![]+[])[+[]]+([![]]+[][[[][(![]+[])[+[]]+([![]]+[][[]])[+!![]+[+[]]]+(![]+[])[!![]+!![]]+(!![]+[])[+[]]+(!![]+[])[!![]+!![]+!![]]+(!![]+[])[+!![]]][(+(+!![]+[+([][(![]+[])[+[]]+([![]]+[][[]])[+!![]+[+[]]]+(![]+[])[!![]+!![]]+(!![]+[])[+[]]+(!![]+[])[!![]+!![]+!![]]+(!![]+[])[+!![]]]+[])[+[]]])+[!![]]+[][(![]+[])[+[]]+([![]]+[][[]])[+!![]+[+[]]]+(![]+[])[!![]+!![]]+(!![]+[])[+[]]+(!![]+[])[!![]+!![]+!![]]+(!![]+[])[+!![]]])[+!![]+[+[]]]+(!!(+([][(![]+[])[+[]]+([![]]+[][[]])[+!![]+[+[]]]+(![]+[])[!![]+!![]]+(!![]+[])[+[]]+(!![]+[])[!![]+!![]+!![]]+(!![]+[])[+!![]]]+[])
...
```

From all the references to `Fuck` and how the contents of `fuck.js` look like we may safely assume that this code is of the variety `JSFuck`.

Using a deobfuscator on the web we will find that the deobfuscator won't work and will return `I'm sorry Dave, I'm afraid I can't do that` as an alert.

Using the program [JSUnfuck](https://github.com/krk/jsunfuck) we are able to reverse this code into a more readable format:
```bash
java -jar jsunfuck-1.1.jar fuck.js --js_output_file unfuck.js
```

Let´s also run this through [Beautifier.io](https://beautifier.io/) to indent the code:
```javascript
[]["f" + ([!1] + [][
    [
        [].filter["constructor"]("return(isNaN+false).constructor." + 420302320151902["toString"]("31")["split"](3)["join"]("C") + "de(" + "119false97false116false101false118false114false123false106false97false118false97false115false99false114false105false112false116false95false105false115false95false97false95false49false97false110false103false117false97false103false51false95false102false48false114false95false105false110false116false51false49false49false51false99false116false117false97false49false115false125" ["split"](!1) +
            ")")()
    ]
])["10"] + "lter"]["constructor"]("return alert")()([].filter["constructor"]("return(isNaN+false).constructor." + 420302320151902["toString"]("31")["split"](3)["join"]("C") + "de(" + "73false39false109false32false115false111false114false114false121false32false68false97false118false101false44false32false73false39false109false32false97false102false114false97false105false100false32false73false32false99false97false110false39false116false32false100false111false32false116false104false97false116" ["split"](!1) +
    ")")());
```

The code is still pretty obfuscated but now we see two pretty interesting strings:
```
73false39false109false32false115false111false114false114false121false32false68false97false118false101false44false32false73false39false109false32false97false102false114false97false105false100false32false73false32false99false97false110false39false116false32false100false111false32false116false104false97false116
```
and 
```
119false97false116false101false118false114false123false106false97false118false97false115false99false114false105false112false116false95false105false115false95false97false95false49false97false110false103false117false97false103false51false95false102false48false114false95false105false110false116false51false49false49false51false99false116false117false97false49false115false125
```

We see that the numbers inside the strings all seem to be within the ASCII-range, so lets try putting these strings into [CyberChef](https://gchq.github.io/CyberChef/) and see what comes out.

Using the recipe `Find / Replace` to replace `false` with spaces and `From decimal` we get the following strings:
```
I'm sorry Dave, I'm afraid I can't do that
```
and
```
watevr{javascript_is_a_1anguag3_f0r_int3113ctua1s}
```

And there we found the flag!

Flag: `watevr{javascript_is_a_1anguag3_f0r_int3113ctua1s}`