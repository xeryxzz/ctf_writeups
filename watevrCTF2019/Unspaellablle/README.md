# Unspaellablle
Category: Miscellaneous

Points: 43

Description: I think the author of this episode's script had a stroke or something... Or maybe it's just me?

We are given a file which contains the script to the TV-show "Stargate SG-1". According to the description, this version of the script is supposed to have misspellings inside of it, so we should probably get the original version of the script. Scavenging the web for the script we are able to find the website https://www.imsdb.com/transcripts/Stargate-SG1-Children-Of-The-Gods.html which has the original script. Let's put this into another file so we can `diff` the files to look for differences:
```bash
$ diff chall.txt orig.txt
17c17
<                are playing a game of cawrds.
---
>                are playing a game of cards.
85c85
<                          I'm telling you, that thinag is moving!
---
>                          I'm telling you, that thing is moving!
114c114
<                A Jaffa comes through the gate and grtabs her. Apophis then comes 
---
>                A Jaffa comes through the gate and grabs her. Apophis then comes
...
```

We start to get these suspicious additions of characters which happens to spell `wat` (as in `watevr`) when extracted.

Let's create a script which extracts these misspellings without any other output:
```python
F1 = 'orig.txt'
F2 = 'chall.txt'

with open(F1, 'r') as f1, open(F2, 'r') as f2:
    for l1, l2 in zip(f1.readlines(), f2.readlines()):
        if l1 != l2:
            for c1, c2 in zip(l1, l2):
                if c1 != c2:
                    print(c2, end='')
                    break
```

We now find that the added characters together spell the flag!

Flag: `watevr{icantspeel_tiny.cc/2qtdez}`