# Blurred Image
Category: Forensics

Points: 194

Description: So I found this cool flag while running around, and decided to take a picture of it. But when I went to show the picture to my friends, it was all blurry! Can you help me get out of this convoluted situation?

The description refers to the word "convoluted", and having basic knowledge of image processing, I suspect that this challenge is about deconvoluting the blurred image in order to read the flag.

In the challenge we are given a blurred image `blurry.png`:

![](blurry.png)

and `logo.png`:

![](logo.png)

I start searching the web and was able to find the program [SmartDeblur](http://smartdeblur.net/download.html). Using SmartDeblur and the correct parameters we are able to extract the flag:
![](SmartDeblur.png)

But that is the fast and lazy solution, I wanted to learn how to do this!

Let's open the image `blurry.png` in an image editor and overlap `logo.png`:

![](overlap.png)

We can see that `logo.png` fits perfectly in the corner. We therefore have an area of `blurry.png` of which the contents are known. That being the case we should able to guess the kernel which was used to blur the image. Using [this](https://stackoverflow.com/a/20824506) Stack Overflow answer describes how to reconstruct the original kernel used for the convolution using known contents of a convoluted image. Let's crop the known area of `blurry.png` and save it in `blurry_crop.png`:

![](blurry_crop.png)

In the Stack Overflow answer we are given the following MATLAB code:
```matlab
%inputs: B,G - gray level blurred and sharp images respectively (double)
%        szKer - 2 element vector specifying the size of the required kernel
%outputs: mKer - the recovered kernel, 
%         imBsynth - the sharp image convolved with the recovered kernel
%
%example usage:  mKer = calcKer(B, G, [11 11]);

function [mKer, imBsynth] = calcKer(B, G, szKer)

  %get the "valid" pixels from B (i.e. those that do not depend 
  %on zero-padding or a circular assumption
  imBvalid = B(ceil(szKer(1)/2):end-floor(szKer(1)/2), ...
      ceil(szKer(2)/2):end-floor(szKer(2)/2));

  %get a matrix where each row corresponds to a block from G 
  %the size of the kernel
  mGconv = im2col(G, szKer, 'sliding')';

  %solve the over-constrained system using MATLAB's backslash
  %to get a vector version of the cross-correlation kernel
  vXcorrKer = mGconv \ imBvalid(:);

  %reshape and rotate 180 degrees to get the convolution kernel
  mKer = rot90(reshape(vXcorrKer, szKer), 2);

  if (nargout > 1)
      %if there is indeed a convolution relationship between B and G
      %the following will result in an image similar to B
      imBsynth = conv2(G, mKer, 'valid');
  end

end
```

In MATLAB we perform the following commands:
```matlab
B = im2double(rgb2gray(imread('blurry_crop.png')));
G = im2double(rgb2gray(imread('logo.png')));

mKer = calcKer(B, G, [5 5]);
```

We start with a relatively small kernel size of 5 by 5. We now have the kernel `mKer` which supposedly is going to help us recover the image. But in order to recover the image we need to find a deconvolution algorithm which fits our needs. Luckily MATLAB comes with some of them built in: `deconvwnr` (Wiener deconvolution), `deconvlucy` (Richardson–Lucy deconvolution), `deconvblind` (blind deconvolution) and `deconvreg` (regularized deconvolution).

Let's import the full `blurry.png` start with using the Wiener deconvolution algorithm:
```matlab
O = im2double(rgb2gray(imread('blurry.png')));
imshow(deconvwnr(O, mKer));
```
And this results in the following image:

![](5.png)

Not quite there. Let's try with some different sizes of the kernel.

10x10:
```matlab
mKer = calcKer(B, G, [10 10]);
imshow(deconvwnr(O, mKer));
```

![](10.png)

15x15:
```matlab
mKer = calcKer(B, G, [15 15]);
imshow(deconvwnr(O, mKer));
```

![](15.png)

Impressive! And there we have the flag!

Flag: `watevr{blur_blur_I_am_a_fish_blur_blur}`