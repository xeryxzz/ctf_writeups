# john_pollard
Category: Cryptography

Points: 500

Description: Sometimes RSA certificates are breakable.

The name of the challenge lead us to the mathematician John Pollard. Reading his work we see that he has invented several algorithms for factorization.

Let's take a look at [Pollard's rho algorithm](https://en.wikipedia.org/wiki/Pollard%27s_rho_algorithm). At the article we are presented with some sample code for Python which implements the algorithm:
```python
def gcd(a: int, b: int) -> int:
    while a % b != 0:
        a, b = b, a % b
    return b


number = 10403
x_fixed = 2
cycle_size = 2
x = 2
factor = 1

while factor == 1:
    count = 1
    while count <= cycle_size and factor <= 1:
        x = (x*x + 1) % number
        factor = gcd(x - x_fixed, number)
        count += 1
    cycle_size *= 2
    x_fixed = x

print(factor)
```

We see that this code needs the `number` variable to be set to the `n` of the certificate. In order to extract `n` from the certificate we can use [RsaCtfTool](https://github.com/Ganapati/RsaCtfTool):
```bash
$ ./RsaCtfTool.py --key cert --dumpkey
[*] n: 4966306421059967
[*] e: 65537
```

We can now populate the `number` variable in our Python program:
```python
...
number = 4966306421059967
...
```

Let's run the code and see what we end up with:
```bash
$ ./john_pollard.py
73176001
```

According to the hints we are supposed to have a `p` and a `q` but we only have one of them. Assuming that only `p` and `q` were used in the generation of the keypair we are able to add another row of code which let's us extract what the other value is:
```python
...
print(number // factor)
```

And we are able to get the other value:
```bash
$ ./john_pollard.py
73176001
67867967
```

We now have the `p` and `q` values and are able to construct the flag!

Flag: `picoCTF{73176001,67867967}`
