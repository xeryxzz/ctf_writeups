# pastaAAA
Category: Forensics

Points: 350

Description: This pasta is up to no good. There MUST be something behind it.

Simply open the image in [StegSolve](www.caesum.com/handbook/Stegsolve.jar) and shift to `Red plane 1` and you should be able to make out the flag:

![](stegsolve.png)

Flag: `picoCTF{paSta_1s_lyf3}`
