# Java Script Kiddie 2
Category: Web Exploitation

Points: 450

Description: The image link appears broken... twice as badly...

This challenge is pretty much the same as [Java Script Kiddie](../Java%20Script%20Kiddie/).

Looking into the Javascript source code we see that the key length is double the size of the key used in Java Script Kiddie.

Let's try `00000000000000000000000000000000` as input:
```
00000000  4e 7e af ce 0d 53 5e 93 ce 78 45 8d 00 51 92 83  |N~¯Î.S^.ÎxE..Q..|
00000010  89 48 af 47 00 36 1e 20 9d 0d e3 00 49 1f 2f 0f  |.H¯G.6. ..ã.I./.|
00000020  00 3a 0e c4 8d d7 3f 36 de 00 00 00 00 93 9b 4b  |.:.Ä.×?6Þ......K|
00000030  a4 20 ae 56 c6 fe 49 0f 74 00 00 0d 9b de fa cd  |¤ ®VÆþI.t....ÞúÍ|
00000040  30 f3 4e 42 30 4c 39 00 00 00 00 00 cc 00 9c 45  |0óNB0L9.....Ì..E|
00000050  33 c6 01 47 8c 82 14 0a 00 78 00 ed a6 00 61 00  |3Æ.G.....x.í¦.a.|
00000060  ea 3f 00 72 d7 0a ef 72 01 03 9c 51 b2 48 f8 52  |ê?.r×.ïr...Q²HøR|
...
```

We see `0x89` in the second row in the first column. From the knowledge we gathered from Java Script Kiddie we could try `10000000000000000000000000000000` as input and see what happens:
```
00000000  89 7e af ce 0d 53 5e 93 ce 78 45 8d 00 51 92 83  |.~¯Î.S^.ÎxE..Q..|
00000010  00 48 af 47 00 36 1e 20 9d 0d e3 00 49 1f 2f 0f  |.H¯G.6. ..ã.I./.|
00000020  a4 3a 0e c4 8d d7 3f 36 de 00 00 00 00 93 9b 4b  |¤:.Ä.×?6Þ......K|
00000030  30 20 ae 56 c6 fe 49 0f 74 00 00 0d 9b de fa cd  |0 ®VÆþI.t....ÞúÍ|
00000040  33 f3 4e 42 30 4c 39 00 00 00 00 00 cc 00 9c 45  |3óNB0L9.....Ì..E|
00000050  ea c6 01 47 8c 82 14 0a 00 78 00 ed a6 00 61 00  |êÆ.G.....x.í¦.a.|
00000060  14 3f 00 72 d7 0a ef 72 01 03 9c 51 b2 48 f8 52  |.?.r×.ïr...Q²HøR|
...
```
As expected we see that `0x89` moves into the correct place. Let's continue solving the second column with the key `18000000000000000000000000000000`:
```
00000000  89 7e af ce 0d 53 5e 93 ce 78 45 8d 00 51 92 83  |.~¯Î.S^.ÎxE..Q..|
00000010  00 48 af 47 00 36 1e 20 9d 0d e3 00 49 1f 2f 0f  |.H¯G.6. ..ã.I./.|
00000020  a4 3a 0e c4 8d d7 3f 36 de 00 00 00 00 93 9b 4b  |¤:.Ä.×?6Þ......K|
00000030  30 20 ae 56 c6 fe 49 0f 74 00 00 0d 9b de fa cd  |0 ®VÆþI.t....ÞúÍ|
00000040  33 f3 4e 42 30 4c 39 00 00 00 00 00 cc 00 9c 45  |3óNB0L9.....Ì..E|
00000050  ea c6 01 47 8c 82 14 0a 00 78 00 ed a6 00 61 00  |êÆ.G.....x.í¦.a.|
00000060  14 3f 00 72 d7 0a ef 72 01 03 9c 51 b2 48 f8 52  |.?.r×.ïr...Q²HøR|
...
```
Interesting... The output doesn't seem to have changed. Let's try `10800000000000000000000000000000` instead:
```
00000000  89 50 af ce 0d 53 5e 93 ce 78 45 8d 00 51 92 83  |.P¯Î.S^.ÎxE..Q..|
00000010  00 00 af 47 00 36 1e 20 9d 0d e3 00 49 1f 2f 0f  |..¯G.6. ..ã.I./.|
00000020  a4 00 0e c4 8d d7 3f 36 de 00 00 00 00 93 9b 4b  |¤..Ä.×?6Þ......K|
00000030  30 10 ae 56 c6 fe 49 0f 74 00 00 0d 9b de fa cd  |0.®VÆþI.t....ÞúÍ|
00000040  33 7c 4e 42 30 4c 39 00 00 00 00 00 cc 00 9c 45  |3|NB0L9.....Ì..E|
00000050  ea d6 01 47 8c 82 14 0a 00 78 00 ed a6 00 61 00  |êÖ.G.....x.í¦.a.|
00000060  14 be 00 72 d7 0a ef 72 01 03 9c 51 b2 48 f8 52  |.¾.r×.ïr...Q²HøR|
...
```
And the second column falls into the correct place!

Let's continue with the key `10804000000000000000000000000000`:
```
00000000  89 50 4e ce 0d 53 5e 93 ce 78 45 8d 00 51 92 83  |.PNÎ.S^.ÎxE..Q..|
00000010  00 00 01 47 00 36 1e 20 9d 0d e3 00 49 1f 2f 0f  |...G.6. ..ã.I./.|
00000020  a4 00 00 c4 8d d7 3f 36 de 00 00 00 00 93 9b 4b  |¤..Ä.×?6Þ......K|
00000030  30 10 44 56 c6 fe 49 0f 74 00 00 0d 9b de fa cd  |0.DVÆþI.t....ÞúÍ|
00000040  33 7c 14 42 30 4c 39 00 00 00 00 00 cc 00 9c 45  |3|.B0L9.....Ì..E|
00000050  ea d6 d0 47 8c 82 14 0a 00 78 00 ed a6 00 61 00  |êÖÐG.....x.í¦.a.|
00000060  14 be 13 72 d7 0a ef 72 01 03 9c 51 b2 48 f8 52  |.¾.r×.ïr...Q²HøR|
...
```

And the third column falls into place! So it seems that the solution is largely similar to Java Script Kiddie, but only every other digit counts to the solution.

With this knowledge we are able to create the key `10804010006090505030403010609060`:
```
00000000  89 50 4e 47 0d 0a 1a 0a 00 00 00 0d 49 48 44 52  |.PNG........IHDR|
00000010  00 00 01 c4 00 00 01 72 01 00 00 00 00 c0 5f 6c  |...Ä...r.....À_l|
00000020  a4 00 00 56 8d 49 44 41 54 78 9c ed 9b 4b 8e a4  |¤..V.IDATx.í.K.¤|
00000030  30 10 44 42 c6 48 2c 41 aa 03 f4 51 cc cd 4a 75  |0.DBÆH,Aª.ôQÌÍJu|
00000040  33 7c 14 47 30 12 5e 96 04 8a 59 d8 a6 eb d3 ad  |3|.G0.^...YØ¦ëÓ.|
00000050  ea d6 d0 72 8c 32 17 88 cf 5b 84 14 b2 9d ce 34  |êÖÐr.2..Ï[..².Î4|
00000060  14 be 13 02 d7 b7 70 c0 78 e3 8d 37 de 78 e3 8d  |.¾..×·pÀxã.7Þxã.|
```
But we still don't end up with an image. It seems that we still are able to change the fourth column since we have another possible `0x47` in that column. Let's try the key `10804010006090505030403010609060`. And there we have the solution! A QR code encoded with the flag appears.

Flag: `picoCTF{e1f443bfe40e958050e0d74aec4daa48}`
