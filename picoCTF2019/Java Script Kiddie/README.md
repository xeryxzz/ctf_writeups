# Java Script Kiddie
Category: Web Exploitation

Points: 400

Description: The image link appears broken...

We are given a link to an web page which contains an empty input field and a submit button. Upon clicking the submit button we get a broken image icon placed on the page, indicating something is wrong with displaying an image.

Inspection of the source code shows us that an array of bytes is fetched upon loading of the page. There is also a function called `assemble_png` which indicates that the finished image should be a PNG image.

The array of bytes is fetched from the path `/bytes` on the server, and the content seems to be static. The server gives us the following data:
```
61 180 159 7 201 26 191 0 0 57 0 231 213 73 8 69 156 215 207 255 127 96 246 10 1 200 0 0 ...
```
It seems like the data is encoded in decimal... Let's copy this data into [CyberChef](https://gchq.github.io/CyberChef/) and inspect what comes out. With the recipe `From decimal` -> `To Hexdump` we get the following output:

```
00000000  3d b4 9f 07 c9 1a bf 00 00 39 00 e7 d5 49 08 45  |=´..É.¿..9.çÕI.E|
00000010  9c d7 cf ff 7f 60 f6 0a 01 c8 00 00 b9 77 22 bb  |.×Ïÿ.`ö..È..¹w"»|
00000020  7b f9 3f 5e e1 0a 86 72 54 d2 00 00 9d 00 49 43  |{ù?^á..rTÒ....IC|
00000030  49 4e 44 cf 79 00 d8 41 80 4b 9c 0d 00 00 1c f9  |INDÏy.ØA.K.....ù|
00000040  2c 50 4e 3b fb 49 b4 54 65 59 a0 00 00 48 86 49  |,PN;ûI´TeY ..H.I|
00000050  cd 00 01 4e e5 02 97 ec 91 f4 05 ed 49 c0 aa 00  |Í..Nå..ì.ô.íIÀª.|
00000060  45 00 00 ae 99 00 df 72 38 de e9 47 00 4d de 52  |E..®..ßr8ÞéG.MÞR|
...
```
Which doesn't look anything like the structure of a PNG.

Let's experiment with the input a little bit. According to the source code it seems like the function wants a 16 digits long input. Let's try `1000000000000000` as input. Upon submission we copy the image URL and paste into CyberChef. Remove the prepending `data:image/png;base64,` from the string and use the recipe `From Base64` -> `To Hexdump` to inspect the data. Now we get the following result:
```
00000000  9c b4 9f 07 c9 1a bf 00 00 39 00 e7 d5 49 08 45  |.´..É.¿..9.çÕI.E|
00000010  7b d7 cf ff 7f 60 f6 0a 01 c8 00 00 b9 77 22 bb  |{×Ïÿ.`ö..È..¹w"»|
00000020  49 f9 3f 5e e1 0a 86 72 54 d2 00 00 9d 00 49 43  |Iù?^á..rTÒ....IC|
00000030  2c 4e 44 cf 79 00 d8 41 80 4b 9c 0d 00 00 1c f9  |,NDÏy.ØA.K.....ù|
00000040  cd 50 4e 3b fb 49 b4 54 65 59 a0 00 00 48 86 49  |ÍPN;ûI´TeY ..H.I|
00000050  45 00 01 4e e5 02 97 ec 91 f4 05 ed 49 c0 aa 00  |E..Nå..ì.ô.íIÀª.|
00000060  89 00 00 ae 99 00 df 72 38 de e9 47 00 4d de 52  |...®..ßr8ÞéG.MÞR|
...
```
Interesting! It seems that the first column of the data has been shifted up by one row.
Let's try with some more inputs:
```
1200000000000000:
00000000  9c f9 9f 07 c9 1a bf 00 00 39 00 e7 d5 49 08 45  |.ù..É.¿..9.çÕI.E|
00000010  7b 4e cf ff 7f 60 f6 0a 01 c8 00 00 b9 77 22 bb  |{NÏÿ.`ö..È..¹w"»|
00000020  49 50 3f 5e e1 0a 86 72 54 d2 00 00 9d 00 49 43  |IP?^á..rTÒ....IC|
00000030  2c 00 44 cf 79 00 d8 41 80 4b 9c 0d 00 00 1c f9  |,.DÏy.ØA.K.....ù|
00000040  cd 00 4e 3b fb 49 b4 54 65 59 a0 00 00 48 86 49  |Í.N;ûI´TeY ..H.I|
00000050  45 0c 01 4e e5 02 97 ec 91 f4 05 ed 49 c0 aa 00  |E..Nå..ì.ô.íIÀª.|
00000060  89 0d 00 ae 99 00 df 72 38 de e9 47 00 4d de 52  |...®..ßr8ÞéG.MÞR|
...
```
First column shifted up by 1 row, second shifted up by 2 rows, the rest shifted up by 0 rows.
```
1234567890123456:
00000000  9c f9 44 3b e5 00 93 c1 db 39 00 00 00 48 aa 52  |.ùD;å..ÁÛ9...HªR|
00000010  7b 4e 4e 4e 99 c6 82 0f af c8 00 0d 00 c0 de 6c  |{NNN.Æ..¯È...ÀÞl|
00000020  49 50 01 ae 42 f0 1a 60 7b d2 9c 00 49 4d 00 e3  |IP.®Bð.`{Ò..IM.ã|
00000030  2c 00 00 47 0d 58 01 3a 41 4b a0 ed 00 6f 00 33  |,..G.X.:AK í.o.3|
00000040  cd 00 46 72 00 5f 44 e3 c0 59 05 47 9b 24 44 89  |Í.Fr._DãÀY.G.$D.|
00000050  45 0c ec 02 6e 26 b2 d3 f9 f4 e9 df b1 10 5f 3f  |E.ì.n&²Óùôéß±._?|
00000060  89 0d a6 1f c7 ca 05 4c 4b de ef 2d 2c ce 6e ef  |..¦.ÇÊ.LKÞï-,Înï|
...
```
First column shifted up by 1 row, second shifted up by 2 rows, third row shifted up by 3 rows, and so on...

Okay, so it seems that the code in `assemble_png` shifts the column `x` up by `n` rows, where `x` is the position of the number `n` in the input string, example:
```
380148027050138
        ^
x = 9, n = 7
```

Let's try repairing the PNG file header using our knowledge of the function and see if we could get an image.

As a reference we grab a sample PNG file header:
```
00000000  89 50 4e 47 0d 0a 1a 0a 00 00 00 0d 49 48 44 52  |.PNG........IHDR|
00000010  00 00 03 06 00 00 01 b1 08 02 00 00 00 57 f4 36  |.............W.6|
00000020  67 00 00 00 01 73 52 47 42 00 ae ce 1c e9 00 00  |g....sRGB.......|
00000030  00 04 67 41 4d 41 00 00 b1 8f 0b fc 61 05 00 00  |..gAMA......a...|
00000040  00 09 70 48 59 73 00 00 16 25 00 00 16 25 01 49  |..pHYs...%...%.I|
00000050  52 24 f0 00 00 ff a5 49 44 41 54 78 5e ec fd 3d  |R$.....IDATx^..=|
00000060  92 1d c7 f1 fd 0f d3 52 28 42 11 0c 19 34 a8 08  |.......R(B...4..|
...
```

Looking into the original data when the columns have not been shifted we find `0x89` at offset `0x70` (or `7` rows from where we expect it to be), which means that the first digit in the key probably will be `7`.

Let's continue in this pattern:
1. In first column `0x89` is `7` rows away
2. In second column `0x50` is `4` rows away
3. In third column `0x4e` is `4` row away
4. In fourth column `0x47` is `7` rows away
5. In fifth column `0x0d` is `8` rows away
6. In sixth column `0x0a` is `2` rows away
7. In seventh column `0x1a` is `9` rows away
8. In eighth column `0x0a` is `1` row away
9. In ninth column `0x00` is `0` rows away
10. In tenth column `0x00` is `8` OR `9` rows away
11. In eleventh column `0x00` is `0` OR `1` OR `2` rows away
12. In twelfth column `0x0d` is `3` rows away
13. In thirteenth column `0x49` is `5` rows away
14. In fourteenth column `0x48` is `4` rows away
15. In fifteenth column `0x44` is `9` rows away
16. In sixteenth column `0x52` is `6` rows away

So we have some uncertainties in the key, and this is because there exists multiple possible positions in which the tenth and eleventh bytes could fit into. Let's feed the key `7447829108035496` and have a closer look at the data:
```
7447829108035496:
00000000  89 50 4e 47 0d 0a 1a 0a 00 00 00 0d 49 48 44 52  |.PNG........IHDR|
00000010  00 00 01 72 00 00 01 72 01 00 00 00 00 c0 5f 6c  |...r...r.....À_l|
00000020  a4 00 00 02 6e 49 44 41 54 00 00 ed 9b 4d 6e e3  |¤...nIDAT..í.Mnã|
00000030  30 0c 46 1f c7 02 b2 54 80 78 9c 47 b1 6f d6 33  |0.F.Ç.²T.x.G±oÖ3|
00000040  cd 0d ec a3 e4 00 05 ec 65 1e a0 df 2c 24 c5 89  |Í.ì£ä..ìe. ß,$Å.|
00000050  8b 41 a6 a8 eb c6 03 72 91 00 05 2d 08 10 14 3f  |.A¦¨ëÆ.r...-...?|
00000060  52 b6 89 cf d8 f0 eb 53 38 1f e9 bc f3 ce 3b ef  |R¶.ÏØðëS8.é¼óÎ;ï|
```

Nice! Now the data starts to look like PNG, but we still can't get an image to show, so we still have improvements to make.

At offset `0x29`, right after `0x49444154` or `IDAT` we see that we could get the data to match our reference PNG more by setting the tenth value of the key from `8` to `9`, moving the `0x78` or `x` up by one row.

Looking at the bottom of the file we see the typical IEND chunk with 4 following bytes after it:
```
7447829108035496:
...
00000260  7b 2a 4f 5e 79 7f b4 4b 7e 4b b2 8b d5 eb 86 bb  |{*O^y.´K~K².Õë.»|
00000270  49 b4 9f cf fb e3 97 fa b9 59 7f 63 b9 49 aa 43  |I´.Ïûã.ú¹Y.c¹IªC|
00000280  2c d7 cf 3b e5 cd df ef 76 f4 42 e7 9d 77 de f9  |,×Ï;åÍßïvôBç.wÞù|
00000290  cd f9 3f 4e 99 1a 93 be ef de 79 00 00 00 00 49  |Íù?N...¾ïÞy....I|
000002a0  45 4e 44 ae 42 60 82 00 00 69 c8                 |END®B`...iÈ|
```
After the 4 bytes we have two bytes set to `0x00`. Knowing that we moved the tenth column up by one row we also know that the third byte following should be `0x00` with our updated key. We could make the assumption that also the fourth byte following should be `0x00`, so we move the eleventh column up by one step.

Applying all this we end up with the key: `7447829109135496`

Let's feed this key to the page and see what we end up with!

A QR code! Simply decode this QR code and we end up with the flag.

Flag: `picoCTF{9e627a851b332d57435b6c32d7c2d4af}`
