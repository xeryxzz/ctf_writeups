# m00nwalk2
Category: Forensics

Points: 300

Description: Revisit the last transmission. We think this transmission contains a hidden message. There are also some clues clue 1, clue 2, clue 3.

Let's begin by decoding the audio files into pictures in a similar fashion to the steps we took in [m00nwalk](../m00nwalk/):

`clue1.wav`:

![](clue1.png)

`clue2.wav`:

![](clue2.png)

`clue3.wav`:

![](clue3.png)

There's no need for decoding `message.wav` since it is identical to the file used in m00nwalk.

Clue 1 suggests that there is a password with the value `hidden_stegosaurus`. The password may also suggest that there is some kind of steganography in play.

Clue 3 gives us the name `Alan Eliasen` and `Future Boy`. Searching for these terms we are lead to a website containing steganography tools things among other stuff. The website says that it uses `steghide` in order to hide and display data, so we can simply run the following commands to get the flag:
```bash
$ steghide extract -sf message.wav -p hidden_stegosaurus
wrote extracted data to "steganopayload12154.txt".
$ cat steganopayload12154.txt
picoCTF{the_answer_lies_hidden_in_plain_sight}
```

Flag: `picoCTF{the_answer_lies_hidden_in_plain_sight}`
