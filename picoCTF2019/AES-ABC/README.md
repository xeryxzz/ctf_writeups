# AES-ABC
Category: Cryptography

Points: 400

Description: AES-ECB is bad, so I rolled my own cipher block chaining mechanism - Addition Block Chaining!

As the description indicates, this challenge involves the ECB block chaining algorithm. [ECB is insecure](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Electronic_Codebook_(ECB)) if the data to be encrypted is greater than the block size of the encryption algorithm.

In the challenge we are given the files `aes-abc.py` (source code responsible for the encryption) and `body.enc.ppm` (an encrypted bitmap image)

Looking into the file `aes-abc.py` we find that the program opens the original message `flag.ppm`, extracts the header and the image data from it, and encrypts the data using `aes_abc_encrypt`. The original header and the encrypted image data is then output into the file `body.enc.ppm`.

In function `aes_abc_encrypt` we find the following:
```python
def aes_abc_encrypt(pt):
    cipher = AES.new(KEY, AES.MODE_ECB)
    ct = cipher.encrypt(pad(pt))

    blocks = [ct[i * BLOCK_SIZE:(i+1) * BLOCK_SIZE] for i in range(len(ct) / BLOCK_SIZE)]
    iv = os.urandom(16)
    blocks.insert(0, iv)

    for i in range(len(blocks) - 1):
        prev_blk = int(blocks[i].encode('hex'), 16)
        curr_blk = int(blocks[i+1].encode('hex'), 16)

        n_curr_blk = (prev_blk + curr_blk) % UMAX
        blocks[i+1] = to_bytes(n_curr_blk)

    ct_abc = "".join(blocks)

    return iv, ct_abc, ct
```

Quick inspection of the code shows that the algorithm uses an unknown `KEY` variable and the ECB mode for encryption of the original message. The `blocks` variable contains the blocks to be chained using the "ABC" algorithm and the first block of the data contains the IV used.

Further inspection shows us that the function is in fact reversible to regular (vulnerable) ECB. This is because of the value of the previous block is added to the current block being encrypted (modulo the maximum possible value of a single block). Addition (in this case) is reversible!

With this knowledge we can develop the following function which reverses the encrypted data into AES-ECB:
```python
def aes_abc_reverse(ct):
    blocks = [ct[i * BLOCK_SIZE:(i+1) * BLOCK_SIZE] for i in range(len(ct) / BLOCK_SIZE)]

    for i in range(len(blocks) - 2, 0, -1): # Go from end to beginning
        prev_blk = int(blocks[i].encode('hex'), 16)
        curr_blk = int(blocks[i+1].encode('hex'), 16)

        n_curr_blk = (curr_blk - prev_blk) % UMAX # Use subtraction instead of addition
        blocks[i+1] = to_bytes(n_curr_blk)

    return ''.join(blocks)
```

We then output this data with the original header to the file `body.ecb.ppm`.

Opening the image we are able to discern the flag.

Flag: `picoCTF{d0Nt_r0ll_yoUr_0wN_aES}`
