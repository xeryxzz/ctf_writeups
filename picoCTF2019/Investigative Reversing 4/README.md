# Investigative Reversing 4
Category: Forensics

Points: 400

Description: We have recovered a binary and 5 images: image01, image02, image03, image04, image05. See what you can make of it. There should be a flag somewhere.

Let's have a look at the binary in Ghidra. We open up Ghidra and import the binary, let it analyze and then we click the function `main` and look at the decompiler:
```c
undefined8 main(void)

{
  size_t sVar1;
  undefined4 local_4c;
  undefined local_48 [52];
  int local_14;
  FILE *local_10;

  flag = local_48;
  local_4c = 0;
  flag_index = &local_4c;
  local_10 = fopen("flag.txt","r");
  if (local_10 == (FILE *)0x0) {
    puts("No flag found, please make sure this is run on the server");
  }
  sVar1 = fread(flag,0x32,1,local_10);
  local_14 = (int)sVar1;
  if (local_14 < 1) {
    puts("Invalid Flag");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  fclose(local_10);
  encodeAll();
  return 0;
}
```

Let's touch `main` up and comment so that it is easier to understand what it is doing:
```c
int main(void)

{
  FILE *flag_file_stream;
  size_t flag_read_result;
  int i;
  char flag_char_array;
  int flag_read_comparison;

  flag = &flag_char_array;
  i = 0;
  flag_index = &i;
  flag_file_stream = fopen("flag.txt","r");  // Open the file "flag.txt" in read mode
  if (flag_file_stream == (FILE *)0x0) { // Return error if file "flag.txt" not found
    puts("No flag found, please make sure this is run on the server");
  }
  flag_read_result = fread(flag,0x32,1,flag_file_stream); // Read 50 bytes into the variable flag
  if ((int)flag_read_result < 1) { // Return error if file "flag.txt" is less than 1 byte
    puts("Invalid Flag");
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  fclose(flag_file_stream); // Close the file stream
  encodeAll(); // Interesting...
  return 0;
}
```

The function `encodeAll` looks interesting, let's have a look at that:
```c
void encodeAll(void)

{
  ulong local_48;
  undefined8 local_40;
  undefined4 not_used_2;
  ulong local_28;
  undefined8 local_20;
  undefined4 not_used_1;
  char i;

  local_28 = 0x635f31306d657449; // "c_10metI"
  local_20 = 0x706d622e70; // "pmb.p"
  not_used_1 = 0;
  local_48 = 0x622e31306d657449; // "b.10metI"
  local_40 = 0x706d; // "pm"
  not_used_2 = 0;
  i = '5';
  while ('0' < i) {
    local_48._0_6_ = CONCAT15(i,(undefined5)local_48);
    local_48 = local_48 & 0xffff000000000000 | (ulong)(uint6)local_48;
    local_28._0_6_ = CONCAT15(i,(undefined5)local_28);
    local_28 = local_28 & 0xffff000000000000 | (ulong)(uint6)local_28;
    encodeDataInFile(&local_48,&local_28,&local_28); // More interesting functions...
    i = i + -1; // Decrement i by 1
  }
  return;
}
```

Looks pretty weird at first look but once we understand that `local_28`, `local_20` and `local_48`, `local_40` are handled as a string, we know what is sent to the `encodeDataInFile` function (`Item01_cp.bmp` and `Item01.bmp`).

Let's have a look at the `encodeDataInFile` function now:
```c
void encodeDataInFile(char *pcParm1,char *pcParm2)

{
  FILE *original_image_file;
  size_t sVar1;
  char original_image_byte;
  char local_2d;
  int local_2c;
  FILE *new_image_file;
  uint local_18;
  int local_14;
  int local_10;
  int local_c;

  original_image_file = fopen(pcParm1,"r"); // Open original image file in read mode
  new_image_file = fopen(pcParm2,"a"); // Open new image file in append mode
  if (original_image_file != (FILE *)0x0) { // If original image is openable
    sVar1 = fread(&original_image_byte,1,1,original_image_file); // Read 1 byte from original image
    local_c = (int)sVar1;
    local_2c = 0x7e3; // 2019
    local_10 = 0;
    while (local_10 < local_2c) { // Write 2019 bytes to new file
      fputc((int)original_image_byte,new_image_file);
      sVar1 = fread(&original_image_byte,1,1,original_image_file);
      local_c = (int)sVar1;
      local_10 = local_10 + 1;
    }
    local_14 = 0;
    while (local_14 < 0x32) { // 0x32 or 50 is max flag length
      if (local_14 % 5 == 0) { // Every fifth iteration
        local_18 = 0;
        while ((int)local_18 < 8) { // Write 8 bytes to file
          local_2d = codedChar((ulong)local_18,(ulong)(uint)(int)*(char *)((long)*flag_index + flag),(ulong)(uint)(int)original_image_byte,(ulong)(uint)(int)*(char *)((long)*flag_index + flag)); // Interesting function...
          fputc((int)local_2d,new_image_file); // Put codedChar into new image file
          fread(&original_image_byte,1,1,original_image_file); // Read new byte from original image
          local_18 = local_18 + 1; // Increment by 1
        }
        *flag_index = *flag_index + 1; // Increment flag index by 1
      }
      else {
        fputc((int)original_image_byte,new_image_file); // Put byte from original image to new image without modification
        fread(&original_image_byte,1,1,original_image_file); // Read new byte from original image
      }
      local_14 = local_14 + 1;
    }
    while (local_c == 1) { // Add the remaining bytes to the new image
      fputc((int)original_image_byte,new_image_file);
      sVar1 = fread(&original_image_byte,1,1,original_image_file);
      local_c = (int)sVar1;
    }
    fclose(new_image_file); // Close files
    fclose(original_image_file); // Close files
    return;
  }
  puts("No output found, please run this on the server");
                    /* WARNING: Subroutine does not return */
  exit(0);
}
```

Now we it seems we only have `codedChar` left:
```c
ulong codedChar(int iParm1,byte bParm2,byte bParm3)

{
  byte local_20;

  local_20 = bParm2;
  if (iParm1 != 0) {
    local_20 = (byte)((int)(char)bParm2 >> ((byte)iParm1 & 0x1f));
  }
  return (ulong)(bParm3 & 0xfe | local_20 & 1);
}
```

Looks like it has to do with a lot of bitwise operations, we should perhaps see patterns of this in the output so let's instead take a look at one of the image files at offset `0x7e3` (`2019`). We'll start off with `Item05_cp.bmp` since this is the first image to go through the encoding process:
```
$ xxd Item05_cp.bmp
...
000007e0: e8e8 e8e8 e8e8 e8e9 e9e9 e8e8 e8e8 e8e9  ................
000007f0: e8e8 e9e8 e9e9 e8e8 e8e8 e8e9 e9e8 e8e8  ................
00000800: e9e9 4e4f 4f4f 4fe9 e9e9 e9e8 e9e9 e8e8  ..NOOOO.........
00000810: e8e8 e8e9 e9e8 e8e8 e8e9 e8e8 e8e8 e8e8  ................
00000820: e8e9 e8e9 e8e9 e8e8 e8e8 e8e8 e9e9 e8e8  ................
00000830: e8e9 e8e8 e8e8 e8e9 e9e8 e9e9 e9e9 e8e8  ................
00000840: e8e8 e8e8 e9e9 e9e8 e8e9 e8e8 e8e8 e8e9  ................
00000850: e8e8 e8e9 e9e8 e8e8 e8e8 e8e8 e8e8 e8e8  ................
00000860: e8e8 e8e8 e8e8 e8e8 e8e8 e8e8 e8e8 e8e8  ................
00000870: e8e8 e8e8 e8e8 e8e8 e8e8 e8e8 e8e8 e8e8  ................
00000880: e8e8 e8e8 e8e8 e8e8 e8e8 e8e8 e8e8 e8e8  ................
...
```

And here emerges a pretty interesting pattern. We can see that the values tend to alternate between even and odd, indicating that some kind of least-significant bit (LSB) steganography might be in play. Extracting the binary sequence where odd byte is 1 and even byte is 0 from offset `0x7e3` we get `00001110 10010110 11000110 11110110`. Keep in mind that after 8 bits has been extracted we need to skip reading 4 bytes in the file since only every fifth iteration writes the encoded bytes to the file. If we reverse these binary strings and encode to ASCII we will see the string `pico` appear!

Perfect! So now we only have to develop some code which extracts the rest of the data from the files:
```python
result = ''
for i in range(5, 0, -1):
    with open('Item0%d_cp.bmp' % i, 'rb') as f:     # Read from file 5 through file 1
        f.seek(0x7e3)                               # Seek to offset
        for _ in range(10):                         # Read 10 encoded bytes (50 bytes flag encoded in 5 files)
            for b in f.read(8):                     # Read 8 bytes
                result += str(b % 2)                # Add binary to result, odd bytes = 1, even bytes = 0
            f.read(4)                               # Skip 4 bytes

result = ''.join(reversed(result))                  # Reverse the string

# Convert from binary to ASCII and print flag
print(''.join(reversed([chr(int(result[i:i + 8], 2)) for i in range(0, len(result), 8)])))
```

Flag: `picoCTF{N1c3_R3ver51ng_5k1115_000000000008a280211}`
