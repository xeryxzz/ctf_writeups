# m00nwalk
Category: Forensics

Points: 250

Description: Decode this message from the moon.

The message we are given is the file `message.wav`, with nothing but what seems like noise. However from experience of working with software defined radio before I recognized that this must be some sort of a radio signal, especially since the description says that it was a message from the moon.

The hints suggest that there is an image being transferred in the capture, so I start searching for ways to receive images over radio communication.

After a while I stumble upon a Wikipedia article about a technique called [Slow-scan television (SSTV)](https://en.wikipedia.org/wiki/Slow-scan_television) and that it was used early in space exploration, fitting well with the description. Further into the article  there are links to several different softwares which are capable of receiving and transmitting SSTV

I picked MMSSTV since I had heard about the software before in my search for the right way parse the data.

Upon opening and trying to import the audio file into MMSSTV, it requests the file extension `.mmv`. Fortunately you are able to simply rename `message.wav` to `message.mmv` in order for MMSSTV to import it.

MMSSTV starts to replay the transmission but nothing shows up in the `RX` panel. The sound file also sounds like it is played back at a slower rate than normal. Looking into the options we see that the default playback rate of MMSSTV is 11025 Hz and not 48000 Hz that the WAV file was stored in. We can resample the file to 11025 Hz using `sox`:
```bash
sox message.mmv -r 11025 -t wav message_11025.mmv
```

Opening the new file `message_11025.mmv` gives us the `RX Mode` of `Scottie 1` and we begin to see an image forming in the `RX` panel. Once the transmission is done we end up with the following in our window:

![](image_1.png)

Shifting over to the `Sync` panel we are able to align the image so that we could actually extract something useful from it. Click the `Slant` button and draw a line along the white line:

![](image_2.png)

Now shift back to the `RX` panel and we are able to see the flag!

![](image_3.png)

Flag: `picoCTF{beep_boop_im_in_space}`
