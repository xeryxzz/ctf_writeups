# b00tl3gRSA2
Category: Cryptography

Points: 400

Description: In RSA d is alot bigger than e, why dont we use d to encrypt instead of e? Connect with nc 2019shell1.picoctf.com 40480.

Looking into the hints of the challenge we see that they question which value `e` typically has in RSA. In most cases `e` is `65537` creating the possibility that `d` is `65537`, since `d` and `e` are swapped.

Connecting to the service we see that the values generated are pretty huge, larger than most programming languages could handle without libraries made to handle these large values:
```
c: 28872060361162122521637495050753271374591210433840427489311114716763846840776220733905946234117970940240643816678808862879521103773213097895406925445276969672433861580945317653141092047774767660224779613777292045720672705971322324826875263789855220024803917761152546297899382508335239569484903386245377187128
n: 88095180259876609574220583980301001743704283157856531796241625814836552882501025060092391066421987159585606153400312842701511995127350464579068504401592906704564193458799226001776716070722824837345114631023012327027095157576039728470640552718522198093704891141117365095978268903309230555942116939586831968967
e: 51341919984527627182924992067497699949658743842628289850884979446689383666434634666985502491447393068959095274869538566412625702334393563858545882869506390739123368660551675287371826315186713825984332251268709368724999576038752193854184266772516109645265928013030830005624086924296385818275567536850686336633
```

We are therefore going to use the library `gmpy2` in Python to decrypt the message. `gmpy2` also comes with many handy functions used in cases such as RSA.

Let's create a simple program that connects to the service and decrypts the message using the value `65537`:
```python
import gmpy2
import socket
import binascii

HOST = '2019shell1.picoctf.com'
PORT = 40480

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    prompt = s.recv(2048)
	# Extract the values c, n and e from the prompt
    c, n, e = [x[1] for x in (y.split(' ') for y in prompt.decode().strip().split('\n'))]

	# Convert from string to gmpy2 values
    c = gmpy2.mpz(c)
    n = gmpy2.mpz(n)
    e = gmpy2.mpz(e)

	# Perform RSA decryption using 65537, convert to hex, and convert to string and print
    print(binascii.unhexlify(hex(gmpy2.powmod(c, 65537, n))[2:]))
```

Flag: `picoCTF{bad_1d3a5_9093280}`
