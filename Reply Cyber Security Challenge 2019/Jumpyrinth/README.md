# Jumpyrinth
Category: Coding

Points: 100

Description: While doing his mission preparation tests, R-boy notices  in the file he's reading that the data has been inserted in a  mysterious order. Read the text with him and discover what's behind it.

We are given two files `2c464e58-9121-11e9-aec5-34415dec71f2.txt` and `RULES.txt`.

`2c464e58-9121-11e9-aec5-34415dec71f2.txt` is a 1 MB large text file filled with seemingly random sequences of data.

`RULES.txt` gives us the rules for parsing the data embedded in `2c464e58-9121-11e9-aec5-34415dec71f2.txt`. It comes with instructions for all the operations needed in order to obtain the flag. So let's get to coding:
```python
import string
from collections import deque

labyrinth_file = '2c464e58-9121-11e9-aec5-34415dec71f2.txt'


class Path:
    __DIR_UP    = 0                                                         # Define some constants for the different directions
    __DIR_RIGHT = 1
    __DIR_DOWN  = 2
    __DIR_LEFT  = 3

    def __init__(self, l, x, y):
    	self.labyrinth = l                                                  # Get a copy of the labyrinth into instance
        self.x = x                                                          # Define start X position
        self.y = y                                                          # Define start Y position
        self.flag = deque()                                                 # Use deque instance as flag container
        self.stack = deque()                                                # Use deque instance as stack container
        self.history = []                                                   # Save previous positions to prevent infinite loops
        self.exit = False                                                   # For graceful exit from Path instance

    def follow(self):
        self.y += 1                                                         # Move down one position after start
        while not self.exit:
            current_position = (self.x, self.y)
            if current_position in self.history:                            # Check for infinite loops
                self.exit = True
            self.history.append(current_position)

            try:
                instruction = self.labyrinth[self.y][self.x]                # Fetch instruction from labyrinth

                if instruction == '@':                                      # @ = Print flag
                    print(''.join(self.flag))
                    return
                elif instruction == '#':                                    # # = Do nothing
                    return
                elif instruction == '(':                                    # Pop from stack and jump n steps to left
                    self.flag.appendleft(self.stack.pop())
                    self.x -= self.get_num(self.__DIR_RIGHT)                # Read number from right
                elif instruction == ')':                                    # Pop from stack and jump n steps to right
                    self.flag.append(self.stack.pop())
                    self.x += self.get_num(self.__DIR_LEFT)                 # Read number from left
                elif instruction == '-':                                    # Remove first char from flag and jump n steps up
                    self.flag.popleft()
                    self.y -= self.get_num(self.__DIR_DOWN)                 # Read number from below
                elif instruction == '+':                                    # Remove last char from flag and jump n steps down
                    self.flag.pop()
                    self.y += self.get_num(self.__DIR_UP)                   # Read number from above
                elif instruction == '%':                                    # Reverse flag string and move down one position
                    self.flag.reverse()
                    self.y += 1
                elif instruction == '[':                                    # Push char from right to stack then jump to right 2 steps
                    self.stack.append(self.labyrinth[self.y][self.x + 1])
                    self.x += 2
                elif instruction == ']':                                    # Push char from left to stack then jump to left 2 steps
                    self.stack.append(self.labyrinth[self.y][self.x - 1])
                    self.x -= 2
                elif instruction == '*':                                    # Push char above to stack then jump up 2 steps
                    self.stack.append(self.labyrinth[self.y - 1][self.x])
                    self.y -= 2
                elif instruction == '.':                                    # Push char below to stack then jump down 2 steps
                    self.stack.append(self.labyrinth[self.y + 1][self.x])
                    self.y += 2
                elif instruction == '<':                                    # Jump n steps to left
                    self.x -= self.get_num(self.__DIR_RIGHT)                # Read number from right
                elif instruction == '>':                                    # Jump n steps to right
                    self.x += self.get_num(self.__DIR_LEFT)                 # Read number from left
                elif instruction == '^':                                    # Jump n steps up
                    self.y -= self.get_num(self.__DIR_DOWN)                 # Read number below
                elif instruction == 'v':                                    # Jump n steps down
                    self.y += self.get_num(self.__DIR_UP)                   # Read number above
                else:
                    return
            except IndexError:
                return

    def get_num(self, direction):                                           # Method for reading numbers from labyrinth
        num = ''
        c = ''

        if direction == self.__DIR_UP:
            i = self.y - 1
            while c in string.digits or c == '':
                num += c
                c = self.labyrinth[i][self.x]
                i -= 1

        if direction == self.__DIR_RIGHT:
            i = self.x + 1
            while c in string.digits or c == '':
                num += c
                c = self.labyrinth[self.y][i]
                i += 1

        if direction == self.__DIR_DOWN:
            i = self.y + 1
            while c in string.digits or c == '':
                num += c
                c = self.labyrinth[i][self.x]
                i += 1

        if direction == self.__DIR_LEFT:
            i = self.x - 1
            while c in string.digits or c == '':
                num += c
                c = self.labyrinth[self.y][i]
                i -= 1
        try:
            return int(num)
        except ValueError:
            self.exit = True
            return 0


def main():
    with open(labyrinth_file, 'r') as f:
        labyrinth = f.readlines()

    for y, l in enumerate(labyrinth):
        start_point = l.find('$')                                           # Find starting point
        if start_point != -1:
            path = Path(labyrinth, start_point, y)                          # Create new Path instance
            path.follow()                                                   # Follow path


if __name__ == '__main__':
    main()
```

We may now execute this with the following command to extract the flag:
```bash
python3 ./jumpyrinth.py | grep FLG:
```

Flag: `{FLG:H4ckItUpH4ckItInL33tM3B3g1n}`
